# Utilisez une image Node.js comme base
FROM node:14

# Définissez le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copiez le fichier package.json et package-lock.json pour installer les dépendances
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez tous les fichiers du projet dans le conteneur
COPY . .

# Construisez l'application React
RUN npm run build

# Exposez le port 3000
EXPOSE 3000

# Commande pour démarrer l'application lorsque le conteneur est lancé
CMD ["npm", "start"]
