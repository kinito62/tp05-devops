import React, { useState, useEffect } from 'react';

function App() {
  const [inputText, setInputText] = useState('');
  const [outputText, setOutputText] = useState('');
  const [fontSize, setFontSize] = useState(16);
  const [color, setColor] = useState('#000000');

  useEffect(() => {
    // Met à jour la taille de la police et la couleur à chaque changement
    document.getElementById('inputField').style.fontSize = `${fontSize}px`;
    document.getElementById('inputField').style.color = color;
  }, [fontSize, color]);

  const handleInputChange = (e) => {
    // Met à jour le texte d'entrée et augmente la taille de la police
    setInputText(e.target.value);
    setFontSize((prevSize) => prevSize + 1);
  };

  const handleButtonClick = async () => {
    // Simulez l'appel à l'API de ChatGPT ici
    const simulatedResponse = await simulateChatGPT(inputText);

    // Met à jour le texte de sortie avec la réponse simulée
    setOutputText(simulatedResponse);

    // Réinitialise le champ d'entrée et réduit la taille de la police
    setInputText('');
    setFontSize(16);
  };

  const simulateChatGPT = async (input) => {
    // Simulez ici l'appel à l'API de ChatGPT
    // Vous devrez remplacer cette fonction avec l'appel réel à l'API

    // Pour cet exemple, nous attendons :apisimplement 1 seconde et renvoyons une réponse statique
    const rep = await fetch('http://gprrrr.co/ollama/api/generate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ 
        model: "mistral",
        prompt: input,
        stream: true,
      }),
    });
    return `Réponse de ChatGPT à : "${rep}"`;
  };

  return (
    <div className="App">
      <h1 style={{ color: '#4285f4' }}>ChatGPT Frontend</h1>
      <div>
        <label style={{ color: '#0000ff' }}>Question:</label>
        <input id="inputField" type="text" value={inputText} onChange={handleInputChange} />
      </div>
      <div>
        <button onClick={handleButtonClick} style={{ backgroundColor: '#0f9d58', color: '#ffffff' }}>Envoyer</button>
      </div>
      <div style={{ marginTop: '20px' }}>
        <strong style={{ color: '#ff0000' }}>Réponse:</strong>
        <p style={{ fontSize: '18px', color: '#00bcd4' }}>{outputText}</p>
      </div>
    </div>
  );
}

export default App;
